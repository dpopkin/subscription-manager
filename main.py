"""
JSON used for parsing subscription file.
"""
# pylint: disable=too-few-public-methods


import argparse
import os.path
import sys
from dotenv import load_dotenv
from src import global_values
from src.parse_subs import ParseSubs as parse
from src.add_sub import AddSub as add
from src.remove_sub import RemoveSub as remove

class RSSDataManager:
    """
    Class where the RSS parser starts.
    """
    def __init__(self):
        parser = argparse.ArgumentParser(
            prog = 'RSSSubManager',
            description = 'Get your RSS from the command line or in HTML!'
        )

        parser.add_argument(
            'action',
            nargs="?",
            default="parse",
            choices=["parse", "add", "remove"],
            help="read subs, add subs, or delete them (default: parse)."
        )

        parser.add_argument(
            '--feed',
            default="",
            required=False,
            help="The feed type for altering an object. Required if using add or remove arguments."
        )

        parser.add_argument(
            '--id',
            default="",
            help="The link, channel ID, or subreddit.\
                    Required if using Add or Remove positional arguments."
        )

        parser.add_argument(
            '-d',
            '--days',
            default=3,
            type=int,
            help="Number of days to parse (default 3, max 15)."
        )

        parser.add_argument(
            '-p',
            '--parse',
            type=str,
            help="Choose one specific feed entry to parse."
        )

        parser.add_argument(
            '-f',
            '--file',
            default=False,
            action='store_true',
            help="Instead of outputting on the command line, create an HTML file.",
        )
        parser.add_argument(
            '-g',
            '--group',
            default=False,
            action='store_true',
            help="Group uploads by feed."
        )
        self.args = parser.parse_args()

    def main(self):
        """
        This method determines which command to run.
        Returns: nothing.
        """
        takeout_file_name = self.get_real_path(global_values.TAKEOUT_FILE_NAME)
        load_dotenv()
        main_file_name = self.get_real_path(global_values.FILE_NAME)
        if(os.path.isfile(takeout_file_name) is False and os.path.isfile(main_file_name) is False):
            print("a subs.json or subscriptions.csv file is required.")
            sys.exit()
        #first_run = (os.path.isfile(takeout_file_name) is True
        #    and os.path.isfile(main_file_name) is False)
        #if first_run:
        #    shared_methods().convert_csv_to_json()
        match self.args.action:
            case "parse":
                parse(self.args).start()
            case "add":
                add(self.args).start()
            case "remove":
                remove().start(self.args)
            case _:
                print("Unknown Error")

    def get_real_path(self, file_name):
        """
        This method gets the path of the working directory.
        Returns: A string with the full path of the working directory.
        """
        return os.path.dirname(os.path.realpath(__file__)) + "/" + file_name

RSSDataManager().main()
