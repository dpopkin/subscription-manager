"""This adds a new sub to the file."""
import json
import defusedxml.ElementTree as ET
from src import global_values
from src.shared_methods import SharedMethods as shared_methods
class AddSub:
    """AddSub Class"""
    def __init__(self, args):
        self.args = args

    def start(self):
        """
        This checks what channel id is being used.
        Returns: nothing.
        """
        if not self.args.id and not self.args.feed:
            print("feed type and ID required.")
        else:
            match self.args.feed:
                case 'youtube':
                    self.add_youtube_item()
                case 'reddit':
                    self.add_reddit_item()
                case _:
                    self.add_generic_item()

    def add_from_direct_link(self):
        """
        This checks that the UUID given is a valid channel.
        Returns: nothing.
        """
        print("Direct link detected, checking if item exists.")
        xml_response = shared_methods().download_xml_or_html_file(self.args.id)
        xml_response = ET.fromstring(xml_response.text)
        if shared_methods().determine_feed_type(xml_response) == 'atom' and\
                self.args.feed != 'reddit':
            atom = global_values.ATOM
            author = xml_response.find(atom + "author").find(atom + "name").text
        else:
            author = xml_response.findtext('.//title', '')
        print("Item exists.")
        self.add_new_sub_to_file(self.args.id, author)

    def add_from_direct_reddit_link(self):
        """
        Add a subreddit to the subs file.
        Returns: Nothing
        """
        response = shared_methods().download_xml_or_html_file(self.args.id)
        subreddit = self.args.id.split("/")[4]
        html = response.text
        rss_tag = html.split('<link rel="alternate" type="application/atom+xml"')
        rss_link = rss_tag[1].split("href=")[1].split("\"")[1].split("\"")[0]
        self.add_new_sub_to_file(rss_link, subreddit)

    def add_from_channel_name(self):
        """
        This checks that the channel exists and 
        parses the channel homepage to get the UUID.
        Existence check is done with the request library.
        Returns: nothing.
        """
        print('Youtube channel name detected. Checking if page exists')
        channel_html = self.download_youtube_channel_page()
        channel_uuid_tag = channel_html.split('<link rel="canonical"')
        channel_uuid = channel_uuid_tag[1].split("href=")[1].split("\"")[1].split("/")
        self.add_new_sub_to_file(
            global_values.FEED_URLS['youtube'] + channel_uuid[len(channel_uuid)-1],
            self.args.id.replace("@", "").split("/")[3]
        )

    def download_youtube_channel_page(self):
        """
        This gets the channel page to get the channel UUID
        Returns: string.
        """
        response = shared_methods().download_xml_or_html_file(self.args.id)
        return response.text

    def add_new_sub_to_file(self, channel_id, author):
        """
        This adds the subscription information to the subs.json file.
        Returns: nothing.
        """
        with shared_methods().open_file(global_values.FILE_NAME) as file:
            parsed_json_data = shared_methods().read_subscription_file(file)
        if self.args.feed not in parsed_json_data:
            parsed_json_data[self.args.feed] = {}
        json_string = json.dumps(parsed_json_data[self.args.feed])
        if json_string.find(channel_id) != -1:
            print("Item already in sub list")
        else:
            with shared_methods().open_write_file(global_values.FILE_NAME) as file:
                parsed_json_data[self.args.feed].append({
                    "url": channel_id,
                    "origin_url": self.args.id if self.args.id != channel_id else "",
                    "name": author
                })
                file.write(json.dumps(parsed_json_data, indent = 2))
            print("Added subscription")

    def add_youtube_item(self):
        """
        This is where logic to set a Youtube item is stored.
        Returns: nothing.
        """
        self.add_from_channel_name()

    def add_reddit_item(self):
        """
        Logic to set reddit items are set here.
        Returns: nothing.
        """
        self.add_from_direct_reddit_link()

    def add_generic_item(self):
        """
        Logic to set news and podcast items are stored here.
        Returns: nothing.
        """
        self.add_from_direct_link()
