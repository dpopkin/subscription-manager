"""All methods related to HTML file generation used in at least two different files go here."""
from datetime import datetime
import os
from string import Template
import nh3
from src import global_values
from src.shared_methods import SharedMethods as shared_methods

class GenerateHtmlFile:
    """Shared methods class."""
    def __init__(self, key, items, args):
        self.args = args
        self.items = items
        self.date_string = os.environ['DATE_STRING']
        self.html_file = ""
        self.key = key

    def start(self):
        """
        This method initialize the HTML file generation and run all steps.
        Retuns: Nothing.
        """
        self.html_file = self.open_html_file()
        self.tokenize_html()

    def open_html_file(self):
        """
        This method opens the template HTML file.
        Returns: the opened HTML file.
        """
        file_name = "html_assets/default.html"
        path = os.path.dirname(os.path.realpath(__file__)) + "/../" + file_name
        return open(path, 'r', encoding="utf-8")

    def create_html_file(self, html, file_name):
        """
        This method will generate the HTML file we put the results in.
        Returns: Nothing.
        """
        with shared_methods().open_write_file(file_name) as file:
            file.write(html)

    def create_content(self):
        """
        This method adds an HTML card for each video.
        Returns: the list of HTML cards.
        """
        cards = []
        card_html = global_values.CARD_HTML
        card_content_html = global_values.CARD_CONTENT_HTML
        global_comment_html = global_values.COMMENT_HTML
        self.items.sort(key=lambda d: d['uploaded'], reverse=True)
        cards.append(Template(global_values.TOPIC_HTML).safe_substitute(feed_type=self.key))
        for item in self.items:
            time = datetime.fromisoformat(nh3.clean(item['uploaded']))
            comment_html = ""
            if item.get('comment', ''):
                comment_html = Template(global_comment_html).safe_substitute(
                    comment=nh3.clean(item.get('comment', '')))
            cards.append(Template(card_content_html if
                                  'reddit' in self.key else card_html).safe_substitute(
                uploader=nh3.clean(item.get('uploader', '')),
                content=nh3.clean(item.get('content', '')),
                link=nh3.clean(item.get('url', '')),
                subreddit = nh3.clean(item.get('subreddit', "")),
                commenthtml= comment_html,
                uploaded=time.astimezone().strftime(self.date_string),
                title=nh3.clean(item.get('title', ''))))
        return cards


    def tokenize_html(self):
        """
        This method will read the html template file
        and insert subscription data.
        Returns: Nothing.
        """
        with self.html_file as file:
            html = file.read()
        now = datetime.today()
        timestamp = now.strftime(self.date_string)
        cards = self.create_content()
        html = Template(html).safe_substitute(date=timestamp, content=''.join(cards))
        self.create_html_file(html, self.key + "-subscriptions.html")
