"""Location of all constants. """
FEED_URLS = {
        "youtube": "https://www.youtube.com/feeds/videos.xml?channel_id=",
        "reddit": "https://old.reddit.com/r/$id/.rss"
}
ENV_BOOL = {
        "False": False,
        "True": True,
        "false": False,
        "true": True
}
CHANNEL_URL = "https://www.youtube.com/"
FILE_NAME = "subs.json"
JSON_ENTRY = "subscriptions"
TAKEOUT_FILE_NAME="subscriptions.csv"
ATOM = "{http://www.w3.org/2005/Atom}"
MEDIA_NAMESPACE = "{http://search.yahoo.com/mrss/}"
TOPIC_HTML = "<div class=\"feed-header\">\
        <h1 class=\"header\">$feed_type</h1>\
        </div>"
CARD_HTML = "<div class=\"card\">\
        <header>\
        <h2>$uploader</h2>\
        </header>\
        <div class=\"content\">\
        <a href=\"$link\" target=\"blank\">\
        <p>$title</p>\
        <p>$uploaded</p>\
        $commenthtml\
        </a>\
        </div>\
        </div>"
CARD_CONTENT_HTML = "<div class=\"card\">\
        <header>\
        r/$subreddit\
        </header>\
        <div class=\"content\">\
        <a href=\"$link\" target=\"_blank\"><p>$title</p></a>\
        <p>$uploaded</p>\
        $content\
        </div>\
        </div>"
COMMENT_HTML = "<a href=\"$comment\" target=\"_blank\">\
        <p>[comments]</p>\
        </a>"
