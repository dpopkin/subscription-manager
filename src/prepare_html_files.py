"""This is where we generate the HTML file when the option is selected."""
# pylint: disable=too-few-public-methods
from src.generate_html_file import GenerateHtmlFile as generate_html_file
class PrepareHtmlFiles:
    """Main HTML file class"""
    def __init__(self, items, args):
        self.args = args
        self.items = items

    def start(self):
        """
        This method initializes the HTML file generation and run all steps.
        Retuns: Nothing.
        """
        for key in self.items.keys():
            for item in self.items[key]:
                if item:
                    generate_html_file(key, self.items[key], self.args).start()
