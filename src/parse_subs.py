"""s is where the sub file is parsed."""
from datetime import datetime
from email.utils import parsedate_to_datetime
import defusedxml.ElementTree as ET
from src import global_values
from src.filter_shorts import FilterShorts as filter_shorts
from src.shared_methods import SharedMethods as shared_methods
from src.output_subs import OutputSubs as output_subs
class ParseSubs:
    """ParseSubs class."""
    def __init__(self, args):
        self.now = datetime.today()
        self.items = {}
        self.args = args
        self.atom = global_values.ATOM
        self.json_entry = global_values.JSON_ENTRY
        self.progress = 0

    def start(self):
        """
        Run the subscription parsing procedure.
        Returns: nothing.
        """
        print("getting subs")
        with shared_methods().open_file(global_values.FILE_NAME) as file:
            parsed_json_data = shared_methods().read_subscription_file(file)
            for key in parsed_json_data.keys():
                self.items[key] = []
            if self.args.parse:
                print("parsing single category")
                number_of_entries = str(len(parsed_json_data[self.args.parse]))
                self.parse_category(self.args.parse, parsed_json_data, number_of_entries)
            else:
                number_of_entries = str(self.determine_total_number_of_feeds(parsed_json_data))
                for category in parsed_json_data:
                    self.parse_category(category, parsed_json_data, number_of_entries)
            output_subs(self.args).start(self.items)

    def request_xml_file(self, sub):
        """
        This gets the feed to give to the service and turns it into parseable data.
        Returns: defusedXML instance.
        """
        link = sub['url']
        xml_file_response = shared_methods().download_xml_or_html_file(link)
        return ET.fromstring(xml_file_response.text)

    def print_progress_bar(self, iteration, total, prefix = '', suffix = ''):
        """
        This is where the progress bar is printed out when parsing.
        Returns: nothing.
        """
        percent = ("{0:." + str(1) + "f}").format(100 * (iteration / float(total)))
        filled_length = int(50 * iteration // total)
        bar_format = '█' * filled_length + '-' * (50 - filled_length)
        print(f'\r{prefix} |{bar_format}| {percent}% {suffix}', end = "")
        # Print New Line on Complete
        if iteration == total:
            print("\n")

    def add_item(self, item, sub, category):
        """
        Adds the item to the list if it passes the filter checks.
        Returns: nothing.
        """
        uploaded_date = item.findtext(self.atom + "published")
        should_add_item = self.check_filters(item, sub, category)
        if should_add_item:
            uploader = item.find(self.atom + "author").findtext(self.atom + "name", "") if\
                    item.find(self.atom + "author") else ""
            self.items[category].append({
                'uploader': uploader,
                'subreddit': sub.get("name", ""),
                'content' : item.findtext(self.atom + "content", ""),
                "title": item.findtext(self.atom + "title"),
                "url": item.find(self.atom + "link").attrib["href"],
                "comment": item.findtext("comments", ""),
                "uploaded": uploaded_date
            })

    def add_rss_item(self, item, sub, category, subs_name):
        """
        Adds the item to the list if it passes the filter checks.
        Returns: nothing.
        """
        uploaded_date = parsedate_to_datetime(item.find("pubDate").text).isoformat()
        should_add_item = self.check_filters(item, sub, category)
        if should_add_item:
            uploader = item.findtext("author", "") or subs_name
            self.items[category].append({
                'uploader': uploader,
                "title": item.findtext("title", ""),
                "url": self.get_rss_link(item, category), 
                "comment": item.findtext("comments", ""),
                "uploaded": uploaded_date
            })



    def check_filters(self, item, sub, category):
        """
        This method calls all filters to see if the video should be added.
        The x day check, title filter, and shorts filter.
        Returns: bool.
        """
        title_filters = sub.get('title_filter', [])
        item_text = item.findtext(self.atom + "title")
        if (
            (any(filters in item_text for filters in title_filters) or
            filter_shorts().start(item, sub, category))):
            return False

        return True

    def determine_total_number_of_feeds(self, feed):
        """
        This method counts the total number of feeds. Used for the progress bar.
        Params: The entire subs.json feed.
        Returns: int.
        """
        total_number = 0
        for feed_entry in feed:
            total_number = total_number + len(feed[feed_entry])
        return total_number

    def get_rss_link(self, item, category):
        """
        This method checks if the item is a podcast and searches for the MP3 link.
        Some Podcasts have both link and audio/enclosure attributes.
        Returns: String.
        """
        pod = 'podcasts'
        if category != pod or (item.find("audio") is None and item.find("enclosure") is None):
            return item.findtext("link")
        return item.find("audio").attrib['src'] if\
            item.find("audio") is not None else item.find("enclosure").attrib['url']

    def parse_category(self, category, parsed_json_data, number_of_entries):
        """
        This method does the parsing of each category. Either a single category or all of them.
        Returns: None.
        """
        for entry in parsed_json_data[category]:
            feed = self.request_xml_file(entry)
            rss_or_atom = shared_methods().determine_feed_type(feed)
            if rss_or_atom == 'atom':
                for item in feed.findall(self.atom + "entry"):
                    uploaded_date = item.findtext(self.atom + "published")
                    iso_date = datetime.fromisoformat(uploaded_date).timestamp()
                    if (self.now.timestamp() - iso_date)/86400 > self.args.days:
                        continue
                    self.add_item(item, entry, category)
            else:
                for item in feed.findall(".//item"):
                    uploaded_date = parsedate_to_datetime(item.find("pubDate").text).isoformat()
                    iso_date = datetime.fromisoformat(uploaded_date).timestamp()
                    if (self.now.timestamp() - iso_date)/86400 > self.args.days:
                        continue
                    self.add_rss_item(item, entry, category, entry['name'])
            self.progress += 1
            suffix_string = "Parsing " + number_of_entries + " feeds"
            self.print_progress_bar(self.progress, int(number_of_entries), suffix_string)
