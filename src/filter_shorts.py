"""This is where all shorts filtering logic is stored."""
import re
import os
from src import global_values
class FilterShorts:
    """FilterShorts class."""
    def __init__(self):
        self.items = []
        self.media = global_values.MEDIA_NAMESPACE
        self.common_youtube_links = ["youtu.be", "www.youtube.com"]
        self.disable_all_short_filtering = global_values.ENV_BOOL[
                os.environ['DISABLE_ALL_SHORT_FILTERING']
        ]
        self.shorts_strings = ["#short", "#shorts"]

    def start(self, video, sub, category):
        """
        This method runs all logic on shorts:
        1. If shorts are allowed globally.
        2. If shorts are allowed for the current channel.
        3. If both preceeding checks are false, check if the video is a short.
        Returns: bool.
        """
        channel_short_filters_disabled = sub.get('disable_channel_short_filtering', False)
        if (
            category != 'youtube' or
            self.disable_all_short_filtering or channel_short_filters_disabled):
            return False
        return self.check_if_short(video)

    def check_if_short(self, video):
        """
        This method runs the many checks that attempt to filter out shorts.
        YouTube RSS feeds include shorts and have no way to discern them from regular videos.
        This method instead tries to take advantage of the homogeneity
            the algo forces on short descriptions and uses the following to attempt filtering:
                empty description,
                the hashtags "#short" or "#shorts",
                hashtags but no other text in the description,
            and a description that does not include a non-YouTube http(s) link.
        This method is far from perfect, but there's no other easy options that use the RSS feed
            and this filter can be toggled on either a global or sub-by-sub basis.
        Returns: bool.
        """
        description = video.find(self.media + "group").findtext(self.media + "description")
        if (
            not description or
            any(filters in description for filters in self.shorts_strings)
            or self.is_only_hashtags(description)
            or self.has_non_yt_link(description)):
            return True
        return False

    def is_only_hashtags(self, description):
        """
        This method checks if the description of a video contains only hashtags.
        A common tactic employeed by Shorts.
        Return: bool.
        """
        description_spaces = description.split(" ")
        for text_part in description_spaces:
            if text_part[0] != "#":
                return False
        return True

    def has_non_yt_link(self, description):
        """
        This method checks if the user has links outside of Youtube in the description.
        Shorts almost never link outside of YouTube.
        Returns: bool.
        """
        for text in re.finditer("http(?:s)?://(?:.+)", description):
            if not any(filters in text.group(0) for filters in self.common_youtube_links):
                return False
        return True
