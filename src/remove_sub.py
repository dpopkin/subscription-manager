"""This removes a sub from the file."""
# pylint: disable=too-few-public-methods
import json
from src.shared_methods import SharedMethods as shared_methods
from src import global_values

class RemoveSub:
    """RemoveSub Class"""
    def start(self, args):
        """
        Removes a sub with a specified or link.
        Returns: nothing.
        """
        if not args.id and not args.feed:
            print("Feed type and ID required.")
        else:
            found_item = 0
            with shared_methods().open_file(global_values.FILE_NAME) as file:
                subscriptions_file = json.load(file)
            subscriptions = subscriptions_file[args.feed]

            for i, subscription in enumerate(subscriptions):
                if subscription.get("origin_url","").find(args.id) != -1 or\
                        subscription.get("url", "").find(args.id) != -1:
                    name = subscription.get('name', '')
                    subscriptions_file[args.feed].pop(i)

                    found_item = 1
                    print('Feed item found')
                    print("removing " + name)
            if not found_item:
                print("Item not in feed list")
            else:
                with shared_methods().open_write_file(global_values.FILE_NAME) as file:
                    file.write(json.dumps(subscriptions_file, indent = 2))
