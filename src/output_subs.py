"""This is where all logic for outputting the results is stored."""
from datetime import datetime
import os
from src.prepare_html_files import PrepareHtmlFiles as prepare_html_files
class OutputSubs:
    """OutputSubs class."""
    def __init__(self, args):
        self.now = datetime.today()
        self.args = args
        self.date_string = os.environ['DATE_STRING']

    def start(self, items):
        """
        Prints out all videos after parsing is complete.
        Returns: nothing.
        """
        if self.args.group and not self.args.file:
            self.print_as_group(items)
        elif self.args.file:
            prepare_html_files(items, self.args).start()
        else:
            self.print_as_normal(items)

    def print_as_normal(self, items):
        """
        Output to command line using standard "uploaded at" rules.
        Returns: nothing.
        """
        print("====== Subscription List Start ======\n\n")
        generation_time = self.now.strftime(self.date_string)
        for item_key in items.keys():
            if items[item_key]:
                items[item_key].sort(key=lambda item: item['uploaded'], reverse=True)
                print(f"======= Start of {item_key} ======")
                for item in items[item_key]:
                    time = datetime.fromisoformat(item.get('uploaded', ""))
                    print("=======")
                    print(item['title'])
                    print(item['url'])
                    if item.get('comment', ''):
                        print(item['comment'])
                    print(item['uploader'])
                    if item.get('subreddit', ''):
                        print(item['subreddit'])
                    print(time.astimezone().strftime(self.date_string))
                    print("=======\n")
                print("\n\n")
        print(f"====== End of list. Generated at: { generation_time } ======")

    def print_as_group(self, items):
        """
        Output to command line in Alphabetical order by uploader and specify uploader.
        Returns: nothing.
        """
        generation_time = self.now.strftime(self.date_string)
        print("====== Grouped Subscription List Start ======\n\n")
        previous_uploader = ""
        for item_key in items.keys():
            for item in items[item_key]:
                items[item_key].sort(key=lambda item: item['uploader'])
                if previous_uploader != item['uploader']:
                    print(f"====== {item['uploader']} ======")
                    previous_uploader = item['uploader']
                print(item['title'])
                print(item['url'])
                if item.get('comment', ''):
                    print(item['comment'])
                if item.get('subreddit', ''):
                    print(item['subreddit'])
                print("\n")

        print("\n\n")
        print(f"====== End of list. Generated at: { generation_time } ======")
