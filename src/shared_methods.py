"""All methods used in at least two different files go here."""
import os
import json
import csv
import requests
from src import global_values

class SharedMethods:
    """Shared methods class."""
    def __init__(self):
        self.file_name = global_values.FILE_NAME
        self.urls = global_values.FEED_URLS

    def open_file(self, file_name):
        """
        Open the subs.json file for reading.
        Returns: instance of readable file.
        """
        path = os.path.dirname(os.path.realpath(__file__)) + "/../" + file_name
        return open(path, 'r', encoding="utf-8")

    def download_xml_or_html_file(self, feed_id):
        """
        This requests the XML file containing the data and returns it.
        Returns: XML data as string.
        """
        feed_string = feed_id
        headers = {
                'User-Agent': os.environ['USER_AGENT']
        }
        response = requests.get(feed_string, timeout=30, headers=headers)
        response.raise_for_status()
        return response

    def open_write_file(self, file_name):
        """
        Opens the subs file for writing.
        Returns: instance of writable file.
        """
        path = os.path.dirname(os.path.realpath(__file__)) + "/../" + file_name
        return open(path, 'w', encoding="utf-8")

    def read_subscription_file(self, file):
        """
        This turns the subs.json file into parseable data.
        Returns: json object.
        """
        read_data = file.read()
        return json.loads(read_data)

    def convert_csv_to_json(self):
        """
        This converts a Google takeout CSV to subs.json.
        Returns: nothing.
        """
        print("Takeout CSV detected. Converting to JSON.")
        csv_file_name = global_values.TAKEOUT_FILE_NAME
        path = os.path.dirname(os.path.realpath(__file__)) + "/" + csv_file_name
        fieldnames = ("Channel Id","Channel Url","Channel Name")
        entries = []
        with open(path, 'r', encoding="utf-8") as csvfile:
            reader = csv.DictReader(csvfile, fieldnames)
            next(reader, None)
            for row in reader:
                print(row["Channel Name"])
                entry_value = {
                    "id": row["Channel Id"],
                    "url": row["Channel Url"],
                    "name": row["Channel Name"],
                }
                entries.append(entry_value)

        output = {
            "subscriptions": entries
        }

        with self.open_write_file(self.file_name) as jsonfile:
            json.dump(output, jsonfile, indent = 2)
            jsonfile.write('\n')

        print("Takeout CSV converted sucessfully. Starting as normal.")

    def determine_if_feed_needs_url(self, category):
        """
        This method decides if the feed ID in question needs a full URL.
        YouTube and Reddit don't but direct feeds do.
        Returns: Boolean.
        """
        if category in ('news_feed', 'podcasts'):
            return False
        return True

    def determine_feed_type(self, feed):
        """
        This method determines if the feed is RSS or ATOM.
        Params: the XML feed.
        Returns: String.
        """
        if feed.tag in ('rss', 'rdf'):
            return 'rss'
        return 'atom'
