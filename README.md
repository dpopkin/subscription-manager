## Command Line Subscription Manager

### Demo
![Demo of the script.](demo.gif)

### What is it?
Put simply, this is a simple command line and html-based subscription manager for RSS Feeds that parses a `subs.json` file and displays the information in either the terminal or a simple HTML file.

### OH BOY ANOTHER FRICKIN' RSS/ATOM READER! Why should I use yours?
- YouTube and Reddit support (yes, they have RSS options!). With specialized features for both.
- Keyword filtering for all feeds.
- Shorts filtering support for YouTube on a per channel or global level (WIP, see Shorts section for more details).
- Group YouTube videos by channel in command line. Useful if you subscribe to a channel with multiple uploads a day.
- Ability to subscribe and unsubscribe to new Feeds/Subreddits/YouTube channels without editing the file.
- Generate an HTML file of the subscriptions or display on the command line. Your choice!
- Minimal dependencies. [Poetry](https://python-poetry.org/) to allow easy Python environment managment, [DefusedXML](https://pypi.org/project/defusedxml/) for XML sanitation, [Requests](https://pypi.org/project/requests/) for HTTP requests, [nh3](https://pypi.org/project/nh3/) for HTML sanitation, and [python-dotenv](https://pypi.org/project/python-dotenv/) to manage env files.
- Uses [Poetry](https://python-poetry.org/) for installation. Which takes care of the Python Virtual Environment for you.

### How to Install
- **Python 3.12 or higher is required. But Poetry will take care of that for you.**.
- Clone Repo.
- Install [Poetry](https://python-poetry.org/docs/#installation) if it does not already exist on your system.
- Create, copy, or import a `subs.json` file. You can copy the subs.json.example file to start. The full syntax of the json file can be found in the `subs.json` section of this README.
- Make sure the subs file is in the root directory of the project.
- Make a copy of the .env.example file and rename it to .env.
- Update the .env file to values of your chosing. It's strongly recommended to at least fill in the `User Agent` value, as many sites may block you without it (EX: Reddit).
- Run `poetry run python main.py`.
- (Optional) create alias to run this command from anywhere.

### Usage
`poetry run python main.py [-h] [-d --days] [-f] [-p] [-g] [--feed] [--id] [{ parse, add, remove }]] [channel_id]`

#### Arguments
- `-h`: Print help.
- `-d`: Change the number of days parse uses to consider a item in a feed new (default: 3, max: 15).
- `-f`: Instead of printing the results to a terminal, create HTML files for each feed type.
- `-p`: Parse a specific category in the file. For example (-p Youtube) will only search for new Youtube videos.
- `-g`: Group videos by channel. Used only in terminal mode.

#### Positional Arguments
- `{ parse }`: Open subscription file and query all entries in your subs.json file for new items (default).
- `{ add }`: Add a channel to the subscription file. Can use either the channel ID or friendly name. More details below.
- `{ remove }`: Remove a channel from the subscription file. Must use channel ID.
- `--feed`: The feed key where the item will be added/removed. Required if using add or remove.
- `--id`: The URL of the item to be added/removed. Required if using add or remove. 

### Reddit
Reddit still maintains an RSS feed, but thanks to the API changes, it is *STRONGLY* recommended to give a custom user-agent in the .env file when parsing reddit, else they may block you.

### Add Subscription
If you want to add a sub, you first need to get a link to the feed. Here are common steps to get the feeds:

#### Youtube
Simply go to the main channel page of whoever you want to subscribe to and copy the link of the URL then paste it in the CLI. For example, to subscribe to the Defcon [channel](https://www.youtube.com/@DEFCONConference), you simply type the following and it will automatically get the feed:

ex: `poetry run python main.py add --feed youtube --id "https://www.youtube.com/@defconconference"`

#### Reddit
Reddit is similar to Youtube in that you only need to give the subreddit link to generate a feed. With the one exception being that you MUST use the `old` subdomain. The redesigned Reddit has removed all instances of the rss feed in the subreddit page HTML(but it still exists). I do plan to address this in a future update. Anyway, to subscribe to the /r/netsec community, you would do the following:

`poetry run python main.py add --feed reddit --id "https://old.reddit.com/r/netsec"`

#### Any Other RSS Feeds
You will need to provide a direct feed link for those. An update to just look for feeds on the main page of a site is coming eventually. Anyway, using the RSS feed of the Infosec news organization Recorded Future as an example, you would do the following (the feed key can be anything you want since this is just a generic feed):

`poetry run python main.py add --feed infosec --id "https://therecord.media/feed/"` 

### Remove Subscription
To remove a feed item, simply remove them from the JSON file or run the command `poetry run python main.py remove --feed <feed_key> --id <name_or_url>`.

### Shorts Filtering
This program includes an option to hide YouTube Shorts and is enabled by default. But the feeds it uses to parse YouTube don't have any disernable way to tell regular videos apart from Shorts. Instead, this program opts to filter based on common patterns found in the descriptions of Shorts. It currently checks for the following: blank description, the hashtags #short or #shorts, nothing but hashtags in the description, or a description that doesn't have any non-https YouTube links. This method can cause false positives, so the functionality can be disabled/enabled on a global or sub-by-sub basis.

#### Disable For a Channel
To disable for a specific channel, simply add `"disable_channel_short_filtering": "True"` to the desired channel in subs.json.

#### Global Disable
To disable shorts filtering for all channels, replace `DISABLE_ALL_SHORT_FILTERING = False` with `DISABLE_ALL_SHORT_FILTERING = True` in the .env file.

#### Title Denylist Filter
Let's say you have a feed you like, but periodically they talk about something you dislike (EX: Z) and you never want to see Z in your sub box. With the `title_filter` option you can hide the item. The `title_filter` value is an array of keywords to filter, so going back to our example, You might add something like this to the subs file:
```
{
    "url": "<URL HERE>",
    "name": "Tech 'n Stuff",
    "title_filter": [
        "Z", "Z stock", "Z Y", "A Y Z"
    ]
},
```

### subs.json
As mentioned earlier, this parser uses a subs.json file to get the videos. For now, I strongly recommend creating a backup of the subs.json file. Just in case there's an issue that eats your file. I plan to add failsafes in a future update. Anyway, for those who want to create a file manually, here's an example key, feed item, and the full list of attributes the program understands:

#### Manual Sub File Creation/Manual Subscribe
A key of any kind with a value of array with each subscription being an object in the array. Each sub value has the following:
1. url: the link to the channel. This URL *MUST* be a direct feed to an RSS or ATOM file or it will not work.
2. name: The feed name.
3. origin_url: (optional. Youtube/Reddit only) a human readable link that allows you to directly access a Youtube channel/Subreddit.
4. disable_channel_short_filtering: (optional, youtube only) used to toggle off the shorts hiding.
5. title_filter: (optional) used to hide titles containing a specific string. See above for syntax.
So an example sub file would look like:
```
{
  "youtube": [
    {
      "url": "https://www.youtube.com/feeds/videos.xml?channel_id=UC6Om9kAkl32dWlDSNlDS9Iw",
      "origin_url": "https://www.youtube.com/channel/@defconconference"
      "name": "DEFCONConference"
    }
  ]
}
```
